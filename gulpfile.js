"use strict";

var gulp = require("gulp");
var $ = require("gulp-load-plugins")();
var browserSync = require("browser-sync");
var packageJson = require("./package.json");
$.through = require("through2");
$.fs = require("fs");
$.path = require("path");
$.convert = require('libreoffice-convert').convert;
$.beautify = require('js-beautify').html;
$.getSlug = require('speakingurl');
$.elasticlunr = require('elasticlunr');
$.runSequence = require('run-sequence');
$.cheerio2 = require('cheerio');
$.log = require('fancy-log');
$.project = __dirname.split('/').pop();

function task(task) {
	return require("./gulp-tasks/" + task)(gulp, $, browserSync);
}


gulp.task("content", task("content"));
gulp.task("layout", task("layout"));

gulp.task("images", task("images"));
gulp.task("copy", task("copy"));
gulp.task("styles", task("styles"));
gulp.task("fonts", task("fonts"));
gulp.task("scripts", task("scripts"));
gulp.task("html", task("html"));
gulp.task("browsersync", task("browsersync"));
gulp.task("todo", task("todo"));
gulp.task("clean", task("clean"));
gulp.task("lunr", task("lunr"));
gulp.task(
	"default", function () {
		$.runSequence(
			"clean",
			["html", "fonts", "styles", "scripts", "images", "copy"],
			"lunr",
			"browsersync"
		);
	});
gulp.task(
	"build", function () {
		$.runSequence(
			"html", "fonts", "styles", "scripts", "images", "copy", "lunr"
		);
	});

gulp.task(
	"production", function () {
		$.runSequence(
			"clean", "html", "fonts", "styles", "scripts", "images", "copy", "lunr"
		);
	});
