<?php
header("Access-Control-Allow-Origin: *");
$project_root = dirname(dirname(dirname(__FILE__)));

$page = $_POST['page'];
if( strpos($page, '/en/') ){
  $lang = 'en';
} else {
  $lang = 'pt';
}
$page = explode('/',$page);
$page = $page[count($page)-1];
if( strpos($page, '?cb=') ){
  $page = $page.split('?cb=');
  $page = $page[0];
}
if( substr($page,-5) !== '.html' ){
  $page = $page . 'index.html';
}
$file = '../../app/'.$lang.'/' . $page;
$file_bak = '../../bak/' . $page . '__' . date("Ymd-His");

$unlnk = unlink('../../dist/'.$lang.'/' . $page);


$user = $_POST['username'];
$email = $_POST['useremail'];

$commit = $_POST['commit'];

$new_content = $_POST['content'];
print $new_content;
$fp = fopen($file, 'w');
$fw = fwrite($fp, $new_content);
fclose($fp);
print $fp;
print $fw;

$fp = fopen($file_bak, 'w');
fwrite($fp, $new_content);
fclose($fp);

print $file;
$cd = shell_exec('cd ' . $project_root);
$git_add = shell_exec('/usr/bin/git -c user.name="cleo" -c user.email="cleo@reportsustentabilidade.com.br" add -A');
$git_commit = shell_exec('/usr/bin/git -c user.name="'.$user.'" -c user.email="'.$email.'" commit -am "saved '.str_replace('../../', '', $file).': '.$commit.'"');
$gulp = shell_exec('/usr/bin/node '.$project_root.'/node_modules/.bin/gulp html 2>&1');
print $gulp;

exit;
