var editor;
jQuery(function ($) {

    var CT = {
        init: function () {
            this.isFresh();
            this.auth.init();
        },
        isFresh: function () {
            var curDate = document.lastModified;
            var freshDate = new Date();
            $.ajax({
                url: window.location.href,
                cache: false,
                success: function (data, textStatus, req) {
                    var freshDate = req.getResponseHeader('last-modified');
                    if (freshDate === curDate) {
                        window.location = window.location.href + '?fresh=' + encodeURIComponent(freshDate);
                    }
                }
            });
        },
        auth: {
            init: function () {
                $('<div class="rpt_editor"><div class="editor_commit"><h1>Breve descrição do(s) ajuste(s) realizado(s):</h1><textarea></textarea></textarea></div><div class="editor_user"><div><h1></h1><h2></h2><button class="editor_logout">logout</button></div><button class="editor_login"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve"><path d="M108.1,949.3c-0.6,27.1-23,48.5-50.1,48c-27.1-0.6-48.5-23-48-50.1c5.4-255.2,259.2-418.6,490-418.6c236.2,0,484.6,163.4,490,418.6c0.5,27.1-21,49.5-48,50.1c-27.1,0.5-49.5-21-50.1-48C887.7,750.1,685.2,626.7,500,626.7C314,626.7,112.1,756.4,108.1,949.3"/><path d="M500,493c108.2,0,196.2-88,196.2-196.2c0-108.1-88-196.2-196.2-196.2c-108.1,0-196.1,88-196.1,196.2C303.9,405,391.8,493,500,493 M500,591.1c-162.5,0-294.2-131.7-294.2-294.2C205.8,134.4,337.5,2.7,500,2.7c162.5,0,294.3,131.7,294.3,294.2C794.2,459.4,662.5,591.1,500,591.1L500,591.1L500,591.1z"/></svg></button></div></div>').appendTo('body');
                $('<link rel="stylesheet" href="../editor/editor.css">').appendTo('head');
                $.ajax({
                    url: "https://www.gstatic.com/firebasejs/4.9.1/firebase.js",
                    dataType: "script",
                    success: function (data) {
                        $('<script>firebase.initializeApp({apiKey: "AIzaSyB-Uy45Ufi9zUND5uutHPO0rH4R-uuitD0",authDomain: "contenteditable-1ea47.firebaseapp.com",databaseURL: "https://contenteditable-1ea47.firebaseio.com",projectId: "contenteditable-1ea47",storageBucket: "contenteditable-1ea47.appspot.com",messagingSenderId: "92801928996"});</script>').appendTo('head');
                        CT.auth.status();
                        CT.auth.login();
                    }
                });
            },
            login: function () {
                $('.editor_login').on('click', function () {
                    var provider = new firebase.auth.GoogleAuthProvider();
                    firebase.auth().languageCode = 'pt';
                    firebase.auth().signInWithPopup(provider).then(function (result) {
                        console.log(result);
                    }).catch(function (error) {
                        console.log(error);
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        var email = error.email;
                        var credential = error.credential;
                    });
                });
            },
            status: function () {
                firebase.auth().onAuthStateChanged(function (user) {
                    if (user) {
                        CT.user.init(user);
                        // if( typeof(FS) !== 'undefined' ){
                        //     if( typeof(FS.identify) !== 'undefined' ){
                        //         FS.identify(user.email);
                        //     }                            
                        // }
                        CT.editor();
                    } else {
                        // No user is signed in.
                    }
                });
            }
        },
        user: {
            init: function (user) {
                console.log(user.photoURL);
                $('.rpt_editor').addClass('logged');
                $('.editor_user h1').html(user.displayName);
                $('.editor_user h2').html(user.email);
                $('.editor_login').css({ backgroundImage: 'url(' + user.photoURL + ')' });
            }
        },

        editor: function () {
            window.CKEDITOR_BASEPATH = '../editor/';
            $('article section').attr('contentEditable', true);
            basket.require(
                { url: "../editor/ckeditor.js" },
                { url: "../editor/tidy.js" }
            )
                .then(function () {
                    CKEDITOR.plugins.add('rpt_save',
                        {
                            requires: 'notification',
                            init: function (editor) {
                                var config = editor.config.inlinesave;
                                if (typeof config == "undefined") { // Give useful error message if user doesn't define config.inlinesave
                                    config = {}; // default to empty object
                                }
                                editor.addCommand('rpt_save',
                                    {
                                        exec: function (editor) {
                                            var postData = {};
                                            var payload = '';

                                            // Clone postData object from config and add editabledata and editorID properties
                                            CKEDITOR.tools.extend(postData, config.postData || {}, true); // Clone config.postData to prevent changing the config.
                                            postData.content = CT.parse.init(editor.getData());
                                            postData.editor = editor.container.getId();
                                            postData.username = $('.editor_user h1').text();
                                            postData.useremail = $('.editor_user h2').text();
                                            postData.commit = $('.editor_commit textarea').val();
                                            postData.page = window.location.pathname;

                                            var formData = '';
                                            for (var key in postData) {
                                                formData += '&' + key + '=' + encodeURIComponent(postData[key]);
                                            }
                                            payload = formData.slice(1);

                                            var xhttp = new XMLHttpRequest();
                                            xhttp.onreadystatechange = function () {
                                                if (xhttp.readyState == 4) {
                                                    if (xhttp.status == 200) {
                                                        editor.showNotification('Documento salvo!', "success");
                                                    }
                                                    else {
                                                        editor.showNotification('Ocorreu um erro.', "warning");
                                                    }
                                                }
                                            };
                                            xhttp.open("POST", '../editor/save.php?d=' + Date.now(), true);
                                            xhttp.setRequestHeader("Content-type", 'application/x-www-form-urlencoded; charset=UTF-8');
                                            xhttp.send(payload);
                                        }
                                    });
                                editor.ui.addButton('rpt_save',
                                    {
                                        toolbar: 'document',
                                        label: 'Save',
                                        command: 'rpt_save',
                                        icon: 'rpt_save.svg'
                                    });
                            }
                        });
                });
        },
        parse: {
            html: '',
            init: function (html) {
                this.html = $('<div class="ct_wrap">' + html + '</div>');
                var options = {
                    "show-body-only": true,
                    "indent": "auto",
                    "tab-size": 0,
                    "wrap": 0,
                    "markup": true,
                    "output-xml": false,
                    "output-html": true,
                    "bare": true,
                    "drop-empty-elements": false,
                    "logical-emphasis": true,
                    "merge-divs": false,
                    "merge-spans": false,
                    "preserve-entities": true,
                    "break-before-br": true,
                    "uppercase-tags": false,
                    "uppercase-attributes": false,
                    "drop-font-tags": true,
                    "tidy-mark": false,
                    "vertical-space": "no"
                }
                this.image();
                this.svg();
                this.anchors();
                this.box();
                this.removeAttrs();
                this.slider();
                html = this.html;
                return tidy_html5(html.html(), options);
            },
            image: function () {
                var $pic;
                var $img;
                var src;
                var alt;
                var classname;
                $html = this.html;
                $html.find('picture').each(function (i, pic) {
                    $pic = $(pic);
                    $img = $pic.find('img');
                    src = $img.attr('srcset');
                    alt = $img.attr('alt');
                    classname = $img.attr('class');
                    $img.removeAttr('srcset').attr('src', src);
                    $img.attr('alt', alt);
                    $img.attr('class', classname);
                    $pic.replaceWith($img[0].outerHTML);
                });
                this.html = $html;
            },
            svg: function () {
                $('svg').each(function (i, el) {
                    if ($(el).attr('data-src') !== undefined) {
                        $(el).removeAttr('xmlns xmlns:xlink viewBox width height').empty();
                    }
                });
            },
            anchors: function () {
                $html = this.html;
                $html.find("h1, h2, h3").removeAttr('id');
                this.html = $html;
            },
            box: function () {
                var classlist;
                var $html = this.html;
                $html.find('.box, .collapsible-heading, .accordion-header, .accordion-heading').each(function (i, box) {
                    $(box).find('a > h1, a > h2').eq(0).unwrap('a');
                    $(box).find('.content').eq(0).replaceWith($(box).find('.content').html());
                    $(box).find('arrow').remove();
                    classlist = $(box).attr('class').split(/\s+/);
                    $.each(classlist, function (i, classname) {
                        if (classname.indexOf('box_') !== -1) {
                            $(box).removeClass(classname);
                        }
                    });
                });
                $html.find('.collapsible-heading, .accordion-heading').find('p.ce-element--empty').remove();
                this.html = $html;
            },
            removeAttrs: function () {
                var $html = this.html;
                $html.find('[data-editable]').removeAttr('data-editable');
                $html.find('[data-name]').removeAttr('data-name');
                $html.find('.is_open').removeClass('is_open');
                $html.find('.menu_open').removeClass('menu_open');
                $html.find('.active').removeClass('menu_open');
                $html.find('.show').removeClass('menu_open');
                $html.find('.local_tooltip').remove();
                $html.find('a').removeAttr('rel target');
                $html.find('a[data-rpt]').attr('href', '');
                $html.find('h1,h2,h3').removeAttr('id');
                $html.find('.aos-init').removeClass('aos-init');
                $html.find('.aos-animate').removeClass('aos-animate');
                $html.find('[data-aos]').removeAttr('data-aos');
                $html.find('.stuck').removeClass('stuck');
                $html.find('.submenu').removeAttr('style');
                $html.find('.submenu li').removeAttr('style');

                this.html = $html;
            },
            slider: function () {
                var $html = this.html;
                $html.find('.primary').find('ul').remove();
                this.html = $html;
            }
        }
    }

    CT.init();


});