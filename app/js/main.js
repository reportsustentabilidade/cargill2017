var RPT;
var UTIL;
(function () {
	jQuery(function ($) {
		RPT = {
			vars: {
				state: "",
				scroll_monitor: {}
			},
			init: function () {
				this.images.init();
				this.aside.init();
				this.menu.init();
				this.search.init();
				this.boxes.init();
				this.tables.init();
				// this.animate.init();
			},
			// aside: {
			// 	init: function () {
			// 		if (!$('body').hasClass('page_index')) {
			// 			basket
			// 				.require({ url: '../js/sticky-kit.min.js', unique: 123 })
			// 				.then(function () {
			// 					$('.site-header a[href="#menu"]').eq(0).stick_in_parent({
			// 						parent: 'body',
			// 						offset_top: 10
			// 					});
			// 					if ($(window).width() > 719) {
			// 						$('article nav').stick_in_parent({
			// 							offset_top: $('.site-header button').eq(0).height() * 2
			// 						});
			// 					}
			// 				});
			// 		}
			// 	}
			// },
			menu: {
				init: function () {
					this.toggle();
				},
				toggle: function () {
					$('a[href="#menu"], a.close').on('click', function () {
						$('html').toggleClass('menu_open');
						return false;
					});
				}
			},
			animate: {
				init: function () {
					$('article > section > *').not('.search_results').each(function (i, el) {
						$(el).addClass('hidden animate');
						RPT.vars.scroll_monitor[i] = scrollMonitor.create($(el), -100);
					});
					$.each(RPT.vars.scroll_monitor, function (i, el) {
						el.one('enterViewport', function () {
							$(el.watchItem).removeClass('hidden');
						});
					});
				}
			},
			images: {
				init: function () {
					this.svg();
				},
				svg: function () {
					var func;
					var src;
					var $svg;
					$('svg[data-src]').each(function (i, el) {
						src = $(el).attr('data-src');
						$.ajax({
							url: src,
							dataType: 'html',
							type: 'GET',
							success: function (data) {
								$svg = $(data);
								$svg.attr({ 'data-src': src })
								func = $(el).attr('data-func');
								if (func !== undefined) {
									$svg.attr({ 'data-func': func })
								}
								$(el).replaceWith($svg);
								if (func !== undefined) {
									RPT[func]();
								}
							}
						});
					});
				}
			},
			search: {
				init: function () {
					this.focus();
					if ($('.search_results').length > 0) {
						this.focus();
					}
				},
				focus: function () {
					$('form.search').on('submit', function (ev) {
						if ($(ev.currentTarget).find('input[type="search"]').val() === '') {
							$(ev.currentTarget).find('input[type="search"]').focus();
							return false;
						}
					});
				},
				query: function () {
					var found = "";
					var query = window.location.href;
					if (query.indexOf("?q=") !== -1) {
						query = query.split("?q=");
						query = query[query.length - 1];
						if (query !== undefined) {
							$('input[name="q"]').val(query);
							basket
								.require({ url: "../js/elasticlunr.min.js" })
								.then(function () {
									$.ajax({
										dataType: "json",
										url: "./index.json",
										success: function (data) {
											idx = elasticlunr(function () {
												this.addField("title");
												this.addField("body");
												this.setRef("href");
											});
											$.each(
												data.documentStore.docs,
												function (i, el) {
													idx.addDoc({
														title: el.title,
														body: el.body,
														href: el.href
													});
												}
											);
											window.idx
												.search(query, {})
												.map(function (result) {
													ref = result.doc.href;
													body = result.doc.body;
													title = result.doc.title;
													title = title.split(" - ");
													title = title[0];

													var index = body.search(
														/query/i
													);
													var start = index - 240;
													if (start < 0) {
														start = 0;
													}
													start = body.indexOf(' ', start + 1);
													var end = index + 240;
													if (end > body.length) {
														end = body.length;
													}
													end = body.indexOf(' ', end);

													content = body.slice(
														start,
														end
													);
													if (content.length > 1) {
														found +=
															'<div class="result"><h1><a href="' +
															ref +
															'">' +
															title +
															"</a></h1><p>..." +
															content +
															"...</p></div>";
													}
												});
											if (found.length === 0) {
												if ('en' === $('html').attr('lang')) {
													found = "<p>Sorry, there are no results for <em>" + query + "</em>.</p>"
												} else {
													found = "<p>Desculpe, não encontramos nenhum resultado para sua busca por <em>" + query + "</em>.</p>"
												}
											} else {
												if ('en' === $('html').attr('lang')) {
													found = '<h1>Results for <em>' + query + '</em>:</h1>' + found;
												} else {
													found = '<h1>Resultados da busca para <em>' + query + '</em>:</h1>' + found;
												}
											}

											$(".search_results").html(found);
										}
									});
								});
						}
					}
				}
			},
			boxes: {
				init: function () {
					this.collapsibles();
				},
				collapsibles: function () {
					$('.box.collapsible')
						.find('a')
						.eq(0)
						.on('click', function (el) {
							$(el.currentTarget)
								.parent()
								.toggleClass('is_open');
							return false;
						});
				}
			},
			tables: {
				init: function () {
					$('table')
						.each(function () {
							if ($(this).width() > $('article').width()) {
								$(this).wrap('<div class="table_wrapper"><div></div></div>');
							}
						});
				}
			}
		};

		RPT.init();

	});

})();
