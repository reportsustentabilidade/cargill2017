Available gulp commands:
========================

Layout
------
**`gulp layout`**

Converts PDF file in *assets/layout* to jpg

Conteúdo
--------
**`gulp content`**

Must have LibreOffice installed.

Converts doc or docx file in *assets/content* to html files:

* *assets/content/[filename]_0_raw.html*: converted using LibreOffice
* *assets/content/[filename]_1_inlined.html*: css styles were inlined
* *assets/content/[filename]_2_processed.html*: html was processed
* *assets/content/converted/*.html*: html was split in multiple files according to headings in the document

Dev
---
**`gulp`**

Processes assets and run BrowserSync server.

Guidelines
----------
* never use *id* in html
* wrap menu items in *.menu* element
* wrap editable content in *section* element
* *.lang* must be used to wrap language switcher links
* language switcher links must have 2 letter classes (pt, en, es)
* edit site title in gulp-tasks/html.js:16,17
* add Google Analytics or Google Tag Manager in gulp-tasks/html.js:19,20

Content editor
----------
* server must have php, nodejs and git installed
* project packages must be installed on server
* path to save.php must be specified in contenttools/editor.js:3