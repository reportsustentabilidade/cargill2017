module.exports = function(gulp, $, browserSync) {
	return function() {
		var idx = $.elasticlunr(function () {
			this.setRef('href');
 			this.addField('title');
			this.addField('body');
		});
		var idx_en = $.elasticlunr(function () {
			this.setRef('href');
 			this.addField('title');
			this.addField('body');
		});

		gulp
			.src("dist/pt/*.html")
      .pipe($.through.obj(function (chunk, enc, cb){
				var filename = chunk.path;
				var body = '';
				filename = filename.split('/');
				filename = filename[filename.length-1];
				console.log(filename);
				if( filename !== 'busca.html' && filename !== 'search.html' && filename !== 'styleguide.html' ){
				var doc = $.fs.readFileSync(chunk.path, 'utf8');
				$c = $.cheerio2.load(doc);
				// console.log($c.html());
					$c('main article p').each(function(i,el){
						if( $c(el).html() ){
							$c(el).html( ' ' + $c(el).html() + ' ' );
						}
					});
					$c('main article *').each(function(i,el){
						if( $c(el).html() ){
							$c(el).html( ' ' + $c(el).html() + ' ' );
						}
					});
					body = $c('main article').text();
					body = body.replace(/null/g, ' ').replace(/undefined/g, ' ');
				var document = {
					"href": filename,
					"title": $c('title').text(),
						"body": body
				}
				idx.addDoc(document);
				// console.log(idx);
				$.fs.writeFile('./dist/pt/index.json', JSON.stringify(idx), function (err) {
					if (err) throw err;
					console.log('done');
				});
				}
				cb();
			}));

			gulp
				.src("dist/en/*.html")
	      .pipe($.through.obj(function (chunk, enc, cb){
					var filename = chunk.path;
					filename = filename.split('/');
					filename = filename[filename.length-1];
					console.log(filename);
					var doc = $.fs.readFileSync(chunk.path, 'utf8');
					$c = $.cheerio2.load(doc);
					// console.log($c.html());
					var document = {
						"href": filename,
						"title": $c('title').text(),
						"body": $c('main').text()
					}
					idx_en.addDoc(document);
					// console.log(idx);
					$.fs.writeFile('./dist/en/index.json', JSON.stringify(idx_en), function (err) {
						if (err) throw err;
						console.log('done');
					});
					cb();
				}));
	};
};
