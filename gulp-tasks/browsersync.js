module.exports = function(gulp, $, browserSync) {
	return function() {
		browserSync({
			notify: false,
			server: "dist",
			baseDir: "dist",
			port: 8082,
			files: ["dist/**/*"],
			logPrefix: $.project
		});

		gulp.watch(['app/**/*.html'], ['html', 'todo']);
		gulp.watch(['app/css/**/*.{scss,css}'], ['styles']);
		gulp.watch(['app/js/**/*.js'], ['scripts']);
		gulp.watch(['app/img/**/*.{jpg,jpeg,png,gif,svg}'], ['images']);
		gulp.watch(['app/editor/**/*'], ['copy']);
	}
};
