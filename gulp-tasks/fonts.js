var font2css = require('gulp-font2css').default;

module.exports = function (gulp, $, browserSync) {
	return function () {
		gulp.src('app/fonts/**/*.{otf,ttf,woff,woff2}')
			.pipe(font2css())
			.pipe($.concat('fonts.css'))
			.pipe(gulp.dest('dist/css'))
	}
};
