module.exports = function(gulp, $, browserSync) {
	return function() {
		gulp.src('app/js/**/*.js').pipe($.plumber()).pipe($.uglify()).pipe($.sourcemaps.write('.')).pipe(gulp.dest('dist/js')).pipe($.size({title: 'js'}));
	}
};
