module.exports = function(gulp, $, browserSync){
	return function(){
        var AUTOPREFIXER_BROWSERS = [
            'ie >= 8',
            'ie_mob >= 10',
            'ff >= 30',
            'chrome >= 34',
            'safari >= 7',
            'opera >= 23',
            'ios >= 7',
            'android >= 4.4',
            'bb >= 10'
        ];

        gulp.src('app/css/**/*.scss')
            .pipe($.plumber())
            .pipe($.sourcemaps.init())
            .pipe($.sass({
                precision: 10
            }).on('error', $.sass.logError))
            .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
            .pipe($.if('*.css', $.csso()))
            .pipe($.sourcemaps.write('.'))
            .pipe(gulp.dest('dist/css'))
            .pipe(browserSync.stream())
            .pipe($.size({
                title: 'css'
            }));
    }
};