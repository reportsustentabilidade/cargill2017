module.exports = function(gulp, $, browserSync) {
	return function() {
		var pdf = gulp.src(['app/pdf/**/*']).pipe(gulp.dest('dist/pdf'));
		var editor = gulp.src(['app/editor/**/*']).pipe(gulp.dest('dist/editor'));
		var webp = gulp.src(['app/img/**/*.webp']).pipe(gulp.dest('dist/img'));

		gulp.src([
			'app/*', '!app/*.html',, 'app/index.html'
		], {}).pipe(gulp.dest('dist')).pipe($.size({title: 'copy'}));
	}
};
