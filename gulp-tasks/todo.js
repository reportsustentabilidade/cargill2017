module.exports = function(gulp, $, browserSync){
	return function(){
        gulp.src('app/**/*.html')
            .pipe($.todo())
            .pipe(gulp.dest('./'));
    }
};
