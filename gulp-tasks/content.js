var fs = require("fs");
var convert = require('libreoffice-convert').convert;
var path;

module.exports = function (gulp, $p, browserSync) {
    return function () {
        gulp.src('./assets/content/*.{doc,docx,html}')
            .pipe($p.through.obj(function (chunk, enc, cb) {

                // var docx = fs.readFileSync(chunk.path);
                path = chunk.path.split('.doc').shift();

                // var converted = convert(docx, 'xhtml', undefined, function(err,res){
                //         fs.writeFile(path + '_0_raw.html', res, function(err) {
                //             if(err) {
                //                 return console.log(err);
                //             }
                //         }); 
                //         if(err){
                //             console.log(err);
                //         }
                //         chunk.contents = res;
                //         cb(null, chunk);
                //     });
                cb(null, chunk);
            }))
            .pipe($p.inlineCss())
            .pipe($p.cheerio({
                run: function ($, file) {
                    var PARSER = {
                        init: function () {
                            this.save(path + '_1_inlined.html', $.html());
                            this.headings();
                            this.strongEm();
                            this.images();
                            this.gri();
                            this.tables();
                            this.setLinks();
                            this.markAnnotations();
                            this.removeAttrs();
                            this.removeDivs();
                            this.removeListBullets();
                            this.removeComments();
                            this.removeEmpty();
                            this.save(path + '_2_processed.html', $.html());
                            this.split();
                        },
                        headings: function () {
                            $('p._5f_01Capitulo').each(function (i, el) {
                                $(el).replaceWith('<h1>' + $(el).html() + '</h1>');
                            });

                            $('p._5f_02Subcapitulo').each(function (i, el) {
                                $(el).replaceWith('<h2>' + $(el).html() + '</h2>');
                            });

                            $('p._5f_03Titulo').each(function (i, el) {
                                $(el).replaceWith('<h3>' + $(el).html() + '</h3>');
                            });

                            $('p._5f_04Intertitulo').each(function (i, el) {
                                $(el).replaceWith('<h4>' + $(el).html() + '</h4>');
                            });
                        },
                        strongEm: function () {
                            $('span').each(function (i, el) {
                                if ($(el).css('font-style') == 'italic' && ($(el).css('font-weight') == 'bold' || $(el).css('font-weight') == '700')) {
                                    $(el).replaceWith('<strong><em>' + $(el).html() + '</em></strong>');
                                } else if ($(el).css('font-style') == 'italic') {
                                    $(el).replaceWith('<em>' + $(el).html() + '</em>');
                                } else if ($(el).css('font-weight') == 'bold' || $(el).css('font-weight') == '700') {
                                    $(el).replaceWith('<strong>' + $(el).html() + '</strong>');
                                } else {
                                    $(el).replaceWith($(el).html());
                                }
                            });
                        },
                        images: function () {
                            var src;
                            $('img').each(function (i, el) {
                                src = $(el).attr('src');
                                if ('data:image' === src.substr(0, 10)) {
                                    $(el).remove();
                                }
                            });
                        },
                        gri: function () {
                            $('span._5f_00GRI').each(function (i, el) {
                                $(el).replaceWith('<span class="gri">' + $(el).html() + '</span>');
                            });
                            var replaced = $.root().html().replace(/(\[GRI.*\])/g, '<span class="gri">$1</span>');
                            $.root().html(replaced);
                        },
                        tables: function () {
                            $('colgroup').remove();
                            $('table p').each(function (i, el) {
                                $(el).replaceWith($(el).html());
                            });
                            $('col').remove();
                            $('table, td').removeAttr('width').removeAttr('height').removeAttr('bgcolor');
                            $('tr').removeAttr('valign');

                        },
                        setLinks: function () {
                            $('a[href^="http"]')
                                .attr('target', '_blank')
                                .attr('rel', 'noopener noreferrer');
                        },
                        markAnnotations: function () {
                            var replaced = $.root().html().replace(/(\[ANNOTATION(.*)\])/g, '<span class="annotation" style="color:red;">$1</span>');
                            $.root().html(replaced);
                        },
                        removeAttrs: function () {
                            $('h1,h2,h3,h4,h5,h6,p,ul,li,table,tr,th,td, a').removeAttr('class');
                            $('*').removeAttr('style');
                            $('font').each(function (i, el) {
                                $(el).replaceWith($(el).html());
                            });
                        },
                        removeEmpty: function () {
                            $('*').not('td, th').each(function (i, el) {
                                if ($(el).text().trim() === '') {
                                    $(el).remove();
                                }
                            });
                        },
                        removeDivs: function () {
                            while ($('div').length > 1) {
                                $('div').each(function (i, el) {
                                    $(el).replaceWith($(el).html());
                                });
                            }
                        },
                        removeListBullets: function () {
                            var replaced = $.root().html().replace(/(|)/g, '');
                            $.root().html(replaced);
                        },
                        removeComments: function () {
                            var replaced = $.root().html().replace(/<!--[\s\S]*?-->/g, '');
                            $.root().html(replaced);
                        },
                        split: function () {
                            var html = $.html();
                            var chapters = html.split('<h1');
                            for (i = 0; i < chapters.length; i++) {
                                curChapter = '<h1' + chapters[i];
                                pages = curChapter.split('<h2');
                                for (ii = 0; ii < pages.length; ii++) {
                                    if (ii !== 0) {
                                        curPage = '<h2';
                                    } else {
                                        curPage = '';
                                    }
                                    curPage += pages[ii];
                                    $curPage = $.load(curPage);
                                    if ($curPage('h1').eq(0).length) {
                                        titleEl = $curPage('h1').eq(0);
                                        h1 = titleEl;
                                    } else {
                                        titleEl = $curPage('h2').eq(0);
                                        h2 = titleEl;
                                    }
                                    title = titleEl.find('span').remove().end().find('sup').remove().end().text();

                                    curPage = '<section><article>' + curPage + '</article></section>';

                                    dir = path.split('/').slice(0, -1).join('/') + '/converted/';
                                    filename = $p.getSlug(title);
                                    PARSER.save(dir + filename + '.html', curPage);
                                }
                            }
                        },
                        save: function (fullPath, content) {
                            if (fullPath.indexOf('/.html') !== -1) {
                                fullPath = fullPath.split('.html').slice(0, -1).join('/') + '/' + Date.now() + '.html';
                            }
                            fs.writeFile(fullPath, $p.beautify(content), function (err) {
                                if (err) {
                                    return console.log(err);
                                }
                                console.log("The file was saved: " + fullPath);
                            });
                        }
                    }
                    PARSER.init();
                },
                parserOptions: {
                    normalizeWhitespace: true,
                    decodeEntities: false
                }
            }));
    }
};
