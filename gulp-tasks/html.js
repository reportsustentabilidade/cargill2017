var getSlug = require("speakingurl");
var fs = require("fs");

module.exports = function (gulp, $p, browserSync) {
	return function () {
		gulp.src("app/index.html").pipe($p.newer('dist')).pipe(gulp.dest("dist"));

		gulp.src(["app/{pt,en}/*.html"])
			// .pipe($p.newer('dist'))
			.pipe($p.headerfooter.header("app/.inc/header.html")).pipe($p.headerfooter.footer("app/.inc/footer.html")).pipe($p.cheerio(function ($, file) {
				//this is used in all projects
				var RPT = {
					vars: {
						lang: "",
						available_langs: [],
						menu: {},
						project_title: {
							"pt": "Cargill - Relatório de Sustentabilidade 2017",
							"en": "Cargill - Sustainability Report 2017"
						},
						google_analytics_id: "", //UA-XXXXXXXX-X
						google_tag_manager: ""
					},
					init: function () {
						this.html_attrs.init();
						this.boxes.init();
						this.links.init();
						this.lang.init();
						this.slug.init();
						//this.images.init();
						this.set_title.init();
						this.captions.init();
						this.scripts.init();
					},
					html_attrs: {
						init: function () {
							this.set_nojs();
						},
						set_nojs: function () {
							$("html").addClass("no-js");
							$("head").append("<script>document.documentElement.className = document.documentElement.className.replace(/no-js/g, '') + ' js ';</script>");
						}
					},
					lang: {
						init: function () {
							this.available_langs();
							this.set_html();
							this.menu();
							this.change_lang();
							this.remove_other();
						},
						available_langs: function () {
							var lang;
							$(".menu").find("a").each(function (i, el) {
								lang = $(el).attr("lang");
								if (RPT.vars.available_langs.indexOf(lang) === -1) {
									RPT.vars.available_langs.push(lang);
								}
							});
						},
						set_html: function () {
							var file_path = file.path;
							file_path = file.path.split("/");
							var lang = file_path[file_path.length - 2];
							RPT.vars.lang = lang;
							$("html").attr("lang", RPT.vars.lang);
						},
						remove_other: function () {
							$("[lang]").not('[lang="' + RPT.vars.lang + '"]').remove();
							$("body *[lang]").removeAttr("lang");
						},
						menu: function () {
							var cur_lang;
							var available_langs = RPT.vars.available_langs;
							for (var i = 0; i < available_langs.length; i++) {
								cur_lang = available_langs[i];
								RPT.vars.menu[cur_lang] = [];
								$('.menu a[lang="' + cur_lang + '"]').each(function (i, link) {
									RPT.vars.menu[cur_lang].push($(link).attr("href"));
								});
							}
						},
						change_lang: function () {
							//improve to adapt to available languages
							var translated = RPT.slug.translate();
							if (translated === undefined || translated === 'undefined') {
								translated = 'index';
							}
							// if ("en" === RPT.vars.lang) {
							// 	$(".lang .en").remove();
							// 	$(".lang .pt").attr("href", "../pt/" + translated + ".html");
							// } else {
							// 	$(".lang .pt").remove();
							// 	$(".lang .en").attr("href", "../en/" + translated + ".html");
							// }
							if ("en" === RPT.vars.lang) {
								$(".lang_switch a").attr("href", "../pt/" + translated + ".html");
							} else {
								$(".lang_switch a").attr("href", "../en/" + translated + ".html");
							}
						}
					},
					slug: {
						init: function () {
							this.body_class();
							this.chapter_class();
							this.translate();
						},
						filename: function () {
							var filepath = file.path;
							var filepathSplit = filepath.split("/");
							var filename = filepathSplit[filepathSplit.length - 1];
							return filename;
						},
						get: function () {
							var filenameSplit = this.filename().split(".");
							var name = filenameSplit[0];
							return name;
						},
						body_class: function () {
							var other_lang = this.translate();
							$("body").addClass("page_" + this.get());
							if (other_lang !== undefined) {
								$("body").addClass("page_" + other_lang);
							}
						},
						translate: function () {
							//improve to adapt to available languages
							var menu;
							var menu_alt;
							var translated;
							if ("en" === RPT.vars.lang) {
								menu = RPT.vars.menu.en;
								menu_alt = RPT.vars.menu.pt;
							} else {
								menu = RPT.vars.menu.pt;
								menu_alt = RPT.vars.menu.en;
							}

							if (menu !== undefined) {
								var index = menu.indexOf(RPT.slug.filename());
								if (index !== -1) {
									translated = menu_alt[index];
									if (translated !== undefined) {
										translated = translated.split('.');
										translated = translated[0];
										return translated;
									}
								}
							}
						},
						chapter_class: function () {
							var $cur_menu_item = $('.menu a[href="' + this.filename() + '"]');
							var $chapter_el;
							var distance_from_root = $cur_menu_item.parentsUntil(".menu").length;
							if (4 === distance_from_root) {
								$chapter_el = $cur_menu_item.parent().parent().parent().find("a").eq(0);
							} else if (2 === distance_from_root) {
								$chapter_el = $cur_menu_item;
							}
							if (typeof $chapter_el !== "undefined") {
								$("body").addClass(getSlug("chapter_" + $chapter_el.text()));
							}
						}
					},
					set_title: {
						init: function () {
							var slug = RPT.slug.filename();
							var $title = $('.menu a[href="' + slug + '"]').last();
							if ($title.length !== 0) {
								$("title").html($title.clone().find("span").remove().end().text() + " - " + RPT.vars.project_title[RPT.vars.lang]);
							} else {
								$("title").html(RPT.vars.project_title[RPT.vars.lang]);
							}
							if ("index.html" === slug) {
								$("title").html(RPT.vars.project_title[RPT.vars.lang]);
							}
						}
					},
          captions: {
            init: function(){
              $('table caption').each(function(i,el){
                $(el).attr('align','bottom');
              });
            }
          },
					links: {
						init: function () {
							this.href();
							this.anchors();
						},
						href: function () {
							var slug;
							$('a[href$="#"]').not('a[href="#"]').each(function (i, el) {
								slug = getSlug($(el).text());
								$(el).attr("href", $(el).attr("href") + slug);
							});
							$('a[href=""]').each(function (i, el) {
								slug = getSlug($(el).text());
								$(el).attr("href", slug + ".html");
							});
							$('a[href^="http"]').each(function (i, el) {
								$(el).attr("target", "_blank").attr("rel", "noreferrer noopener");
							});
						},
						anchors: function () {
							var id;
							$("h1, h2, h3").each(function (i, el) {
								id = getSlug($(el).clone().find("span").remove().end().text());
								$(el).attr("id", id);
							});
						}
					},
					// images: {
					// 	init: function () {
					// 		this.picturefill();
					// 	},
					// 	picturefill: function () {
					// 		var src;
					// 		var alt;
					// 		var webp;
					// 		var filename;
					// 		var slug;
					// 		//var ext;
					// 		$("img").each(function(i, el) {
					// 			src = $(el).attr("src");
					// 			alt = $(el).attr("alt");
					// 			filename = src.split("/");
					// 			filename = filename[filename.length - 1];
					// 			filename = filename.split(".");
					// 			filename = filename[0];
					// 			slug = getSlug(filename);
					// 			webp = src.slice(0, -4) + ".webp";
					// 			$(el).wrap('<picture class="' + $(el).attr("class") + '" data-slug="' + slug + '" />');
					// 			$(el).removeAttr("class");
					// 			$(el)
					// 				.removeAttr("src")
					// 				.attr("srcset", src);
					// 			if (src.indexOf("svg") === -1) {
					// 				$(el)
					// 					.parent()
					// 					.prepend('<source srcset="' + webp + '" type="image/webp">');
					// 			}
					// 		});
					// 	}
					// },
					boxes: {
						init: function () {
							this.title();
						},
						slug: function ($el) {
							var $title;
							var slug;
							$title = $el.find("h1").eq(0);
							slug = getSlug($title.text());
							return slug;
						},
						title: function () {
							var _self = this;
							var slug;
							var title;
							$(".box").each(function (i, el) {
								slug = _self.slug($(el));
								$(el).addClass("box_" + slug);
							});
							$(".collapsible-heading").each(function (i, el) {
								slug = _self.slug($(el));
								title = $(el).find("h1").eq(0).html();
								$(el).addClass("box_" + slug);
								$(el).find("h1").eq(0).remove();
								$(el).attr('id', slug);
								$(el).html('<a href="#' + slug + '"><h1>' + title + '</h1></a>');
								$(el).append('<span class="arrow"><svg xmlns="http://www.w3.org/2000/svg" height="4.415mm" width="6.858mm" version="1.1" viewBox="0 0 6.8579998 4.41501"><g transform="translate(-170.03 248.04)"><path d="m170.18-246.69c-0.0998-0.10019-0.14993-0.22154-0.14993-0.36442 0-0.14252 0.0501-0.26423 0.14993-0.36407l0.49283-0.47166c0.10019-0.10019 0.22155-0.14993 0.36442-0.14993 0.14288 0 0.26458 0.0497 0.36442 0.14993l2.0574 2.0574 2.0574-2.0574c0.0998-0.10019 0.22154-0.14993 0.36442-0.14993 0.14252 0 0.26423 0.0497 0.36407 0.14993l0.49318 0.47166c0.0998 0.0998 0.14993 0.22155 0.14993 0.36407 0 0.14288-0.0501 0.26423-0.14993 0.36442l-2.9146 2.9146c-0.10019 0.10019-0.22154 0.15028-0.36442 0.15028-0.14287 0-0.26423-0.0501-0.36442-0.15028z" fill="#fff"/></g></svg></span>');
							})
							$('.accordion-heading').each(function (i, el) {
								$title = $(el).find("h2").eq(0);
								slug = getSlug($title.text());
								$(el).addClass("box_" + slug);
								$(el).find("h2").eq(0).wrap('<a href="#' + slug + '"></a>');
								$(el).attr('id', slug);
								$(el).append('<span class="arrow"><svg xmlns="http://www.w3.org/2000/svg" height="4.415mm" width="6.858mm" version="1.1" viewBox="0 0 6.8579998 4.41501"><g transform="translate(-170.03 248.04)"><path d="m170.18-246.69c-0.0998-0.10019-0.14993-0.22154-0.14993-0.36442 0-0.14252 0.0501-0.26423 0.14993-0.36407l0.49283-0.47166c0.10019-0.10019 0.22155-0.14993 0.36442-0.14993 0.14288 0 0.26458 0.0497 0.36442 0.14993l2.0574 2.0574 2.0574-2.0574c0.0998-0.10019 0.22154-0.14993 0.36442-0.14993 0.14252 0 0.26423 0.0497 0.36407 0.14993l0.49318 0.47166c0.0998 0.0998 0.14993 0.22155 0.14993 0.36407 0 0.14288-0.0501 0.26423-0.14993 0.36442l-2.9146 2.9146c-0.10019 0.10019-0.22154 0.15028-0.36442 0.15028-0.14287 0-0.26423-0.0501-0.36442-0.15028z" fill="#fff"/></g></svg></span>');
							});
						}
					},
					scripts: {
						init: function () {
							this.basket();
							this.google_analytics();
							this.google_tag_manager();
						},
						basket: function () {
							var script = "<script>(function () {basket.require(";
							var src;
							var now = Date.now();
							$("<script>(function(){basket.require({ url: '../css/fonts.css', key: '" + RPT.vars.project_title.pt.split(' ')[0] + "_fonts', execute: false }).then(function(responses) { var css = responses[0].data; _stylesheet.appendStyleSheet(css, function(){});});})();</script>").appendTo("body");
							$("body script[src]").not('[src*="basket"]').each(function (i, tag) {
								src = $(tag).attr("src");
								if (i !== 0) {
									script += ",";
								}
								script += "{";
								script += "url: '" + src + "'";
								if (src.indexOf("main") !== -1) {
									script += ", skipCache: true";
								}
								script += ", unique: " + now;
								script += ", key: '" + RPT.vars.project_title.pt.split(' ')[0] + "_" + src + "'";
								script += "}";
								$(tag).remove();
							});
							script += ")})()</script>";
							$("body").append(script);
						},
						google_analytics: function () {
							if (RPT.vars.google_analytics_id !== "") {
								$("<script>(function (i, s, o, g, r, a, m) {i['GoogleAnalyticsObject'] = r;i[r] = i[r] || function () {(i[r].q = i[r].q || []).push(arguments)}, i[r].l = 1 * new Date();a = s.createElement(o),m = s.getElementsByTagName(o)[0];a.async = 1;a.src = g;m.parentNode.insertBefore(a, m)})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');ga('create', '" + RPT.vars.google_analytics_id + "', 'auto');ga('send', 'pageview');</script>").appendTo("body");
							}
						},
						google_tag_manager: function () {
							if (RPT.vars.google_tag_manager !== "") {
								$("<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','" + RPT.vars.google_tag_manager + "');</script>").appendTo('header');
								$('<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=' + RPT.vars.google_tag_manager + '" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>').prependTo('body');
							}
						}
					}
				};

				//this is specific for this project
				var CH = {
					init: function () {
						this.header.init();
						this.nav_aside.init();
						this.siblings.init();
					},
					header: {
						init: function () {
							var $item = $('#menu a[href="' + RPT.slug.filename() + '"]');
							if ($item.length) {
								$('.site-header > div > div > div:nth-of-type(2) > section.titlearea > h1').html($item.text());
								//if ($item.attr('data-desc')) {
									//$('<div class="heading"><p>' + $item.attr('data-desc') + '</p></div>').prependTo('.main');
								//}
							}
							$('#menu a').each(function (i, el) {
								$(el).removeAttr('data-desc');
							});
						}
					},
					nav_aside: {
						init: function () {
							var title;
							var html = '';
							$('main > article > section h1').not('.article-header > div > h1').each(function (i, el) {
								title = $(el).clone().find('span').remove().end().text();
								html += '<li><a href="#' + getSlug(title) + '">' + title + '</a></li>';
							});
							if (html.length > 0) {
								html = '<nav><ul>' + html + '</ul></nav>';
							}
							$('main > article').prepend(html);
						}
					},
					siblings: {
						init: function () {
							var slug = RPT.slug.filename();
							var $prev = $('#menu a[href="' + slug + '"]').parent().prev('li').find('a');
							var $next = $('#menu a[href="' + slug + '"]').parent().next('li').find('a');
							var prev_html;
							var next_html;
							if (null === $prev.html()) {
								prev_html = $('<a href="#" class="hidden"></a>')
							} else {
								prev_html = '<a href="' + $prev.attr('href') + '"><span>' + $prev.html() + '</span></a>';
							}

							if (null === $next.html()) {
								next_html = $('<a href="#" class="hidden"></a>')
							} else {
								next_html = '<a href="' + $next.attr('href') + '"><span>' + $next.html() + '</span></a>';
							}
							$('.main').append('<nav class="article-siblings">' + prev_html + next_html + '</nav>');
						}
					}
				};

				RPT.init();
				CH.init();
			})).pipe($p.if("*.html", $p.htmlmin({ collapseWhitespace: true }))).pipe(gulp.dest("dist")).pipe($p.size({ title: "html" }));
	};
};
