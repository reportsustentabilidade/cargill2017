module.exports = function (gulp, $, browserSync) {
	return function () {
		gulp.src("app/img/*.{jpg,png,gif,svg}")
			.pipe($.plumber())
			.pipe($.newer('dist/img'))
			.pipe($.imagemin({
				progressive: true,
				interlaced: true,
				svgoPlugins: [
					{
						removeViewBox: true
					}
				]
			}))
			.pipe(gulp.dest("dist/img"))
			.pipe($.webp())
			.pipe(gulp.dest("dist/img"))
			.pipe($.size({title: "img"}));


	// 	gulp.src("app/img/*.{jpg,png,gif}")
	// 		.pipe($.plumber())
	// 		// .pipe($.newer('dist/img'))
	// 		.pipe($.responsive({
	// 			"*.{jpg,png,gif}": [
	// 				{
	// 					width: 1920
	// 				},
	// 				{
	// 					width: 1920,
	// 					rename: {
	// 						extname: '.webp'
	// 					}
	// 				},
	// 				{
	// 					width: 1280,
	// 					rename: {
	// 						suffix: '-1280'
	// 					}
	// 				},
	// 				{
	// 					width: 1280,
	// 					rename: {
	// 						suffix: '-1280',
	// 						extname: '.webp'
	// 					}
	// 				},
	// 				{
	// 					width: 960,
	// 					rename: {
	// 						suffix: '-960'
	// 					}
	// 				},
	// 				{
	// 					width: 960,
	// 					rename: {
	// 						suffix: '-960',
	// 						extname: '.webp'
	// 					}
	// 				},
	// 				{
	// 					width: 768,
	// 					rename: {
	// 						suffix: '-768'
	// 					}
	// 				},
	// 				{
	// 					width: 768,
	// 					rename: {
	// 						suffix: '-768',
	// 						extname: '.webp'
	// 					}
	// 				},
	// 				{
	// 					width: 420,
	// 					rename: {
	// 						suffix: '-420'
	// 					}
	// 				},
	// 				{
	// 					width: 420,
	// 					rename: {
	// 						suffix: '-420',
	// 						extname: '.webp'
	// 					}
	// 				}
	// 			]
	// 		}, {
	// 				quality: 82,
	// 				compressionLevel: 7,
	// 				progressive: true,
	// 				withMetadata: false,
	// 				withoutEnlargement: true,
	// 				skipOnEnlargement: false,
	// 				errorOnEnlargement: false,
	// 			}))
	// 		.pipe(gulp.dest("dist/img"))
	// 		.pipe($.size({ title: "img" }));
 	};
};
