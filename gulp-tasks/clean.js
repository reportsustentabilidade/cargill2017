var del = require('del');
module.exports = function(gulp, $, browserSync){
	return function(){
        return del.sync([
            'dist/*'
        ]);
    }
};
