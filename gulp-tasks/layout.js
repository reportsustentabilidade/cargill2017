module.exports = function(gulp, $, browserSync) {
	return function() {
		return $.run('convert ./assets/layout/*.pdf -background white -alpha remove ./assets/layout/layout-%d.jpg').exec().pipe(gulp.dest('output'));
	}
};